package main

import (
	"bytes"
	"compress/zlib"
	"encoding/base64"
	"io/ioutil"
	"log"
	"strings"
	"text/template"
)

type Base64TemplateState struct {
	VideoBase64 string
	AudioBase64 string
}

func withRawBase64() {

	templateSrc, err := ioutil.ReadFile("template.rawbase64.html")
	if err != nil {
		log.Fatal(err)
	}

	/*
		tmpl, err := template.New("test").Parse(string(templateSrc))

		if err != nil {
			log.Fatal(err)
		}
	*/

	audiobase64, err := readaudio()

	if err != nil {
		log.Fatal(err)
	}

	videobase64, err := readvideo()

	if err != nil {
		log.Fatal(err)
	}
	//var b bytes.Buffer

	state := Base64TemplateState{
		AudioBase64: audiobase64,
		VideoBase64: videobase64,
	}

	templateSrcStr := string(templateSrc)

	replacer := strings.NewReplacer("{{.AudioBase64}}", state.AudioBase64, "{{.VideoBase64}}", state.VideoBase64)

	result := replacer.Replace(templateSrcStr)

	ioutil.WriteFile("output.html", []byte(result), 0666)
}

type ZlibTemplateState struct {
	PakoSource      string
	VideoBase64Zlib string
	AudioBase64Zlib string
}

func withBase64AndZlib() {

	templateSrc, err := ioutil.ReadFile("template.html")
	if err != nil {
		log.Fatal(err)
	}

	tmpl, err := template.New("test").Parse(string(templateSrc))

	if err != nil {
		log.Fatal(err)
	}

	zlibaudiobase64, err := readaudioWithZlib()

	if err != nil {
		log.Fatal(err)
	}

	zlibvideobase64, err := readvideoWithZlib()

	if err != nil {
		log.Fatal(err)
	}

	pakojs, err := ioutil.ReadFile("pako.min.js")

	if err != nil {
		log.Fatal(err)
	}

	var b bytes.Buffer

	state := ZlibTemplateState{
		PakoSource:      string(pakojs),
		VideoBase64Zlib: zlibvideobase64,
		AudioBase64Zlib: zlibaudiobase64,
	}

	tmpl.Execute(&b, state)

	ioutil.WriteFile("output.html", b.Bytes(), 0666)
}

func readaudioWithZlib() (string, error) {
	data, err := ioutil.ReadFile("audio.mp3")
	if err != nil {
		return "", err
	}
	audiostr := base64.RawStdEncoding.EncodeToString(data)

	ioutil.WriteFile("audio.base64", []byte(audiostr), 0666)

	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write([]byte(audiostr))
	zlibaudio := b.Bytes()

	ioutil.WriteFile("audio.base64.zlib", []byte(zlibaudio), 0666)

	zlibaudiobase64 := base64.RawStdEncoding.EncodeToString(zlibaudio)

	ioutil.WriteFile("audio.base64.zlib.base64", []byte(zlibaudiobase64), 0666)

	return zlibaudiobase64, nil
}

func readvideoWithZlib() (string, error) {
	videodata, err := ioutil.ReadFile("video.webm")

	if err != nil {
		return "", err
	}

	videostr := base64.RawStdEncoding.EncodeToString(videodata)

	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write([]byte(videostr))
	zlibvideo := b.Bytes()

	zlibvideobase64 := base64.RawStdEncoding.EncodeToString(zlibvideo)
	return zlibvideobase64, nil
}

func readaudio() (string, error) {
	data, err := ioutil.ReadFile("audio.mp3")
	if err != nil {
		return "", err
	}
	audiostr := base64.StdEncoding.WithPadding(base64.StdPadding).EncodeToString(data)

	return audiostr, nil
}

func readvideo() (string, error) {
	videodata, err := ioutil.ReadFile("video.webm")

	if err != nil {
		return "", err
	}

	videostr := base64.StdEncoding.WithPadding(base64.StdPadding).EncodeToString(videodata)

	return videostr, nil
}

func main() {
	withRawBase64()
}
